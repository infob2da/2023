---
layout: post
title:  "Archiving 2020 INFOB2DA content"
date:   2021-06-08 15:10:56 +0900
categories: website update
---
# 2020
The INFOB2DA course content of 2020 can be found [here]({{ site.baseurl }}/2020/).
