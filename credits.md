---
layout: page
title: Credits
permalink: /credits/
---

This course is inspired by the [Data Analysis and Visualization Course](https://www.vis.uni-konstanz.de/en/teaching) at the University of Konstanz, Germany.
We are grateful for their support and borrowed parts of the ANAVIS course material taught by **[Professor Daniel Keim](https://research.dbvis.de)** and **[Dr. Johannes Fuchs](https://www.vis.uni-konstanz.de/en/persons/fuchs)** at the Konstanz University, Germany.

We have heavily drawn on materials and examples found online and tried our best to give credit by linking to the original source. Please contact us if you find materials where the credit is missing or that you would rather have removed.

Futhermore, some of the material in this course is based on the classes taught by **Alexander Lex** at the University of Utah, **Carlos Scheidegger** at the University of Arizona, **Marc Streit** at JKU Linz, **Pat Hanrahan** at Stanford, **Jeff Heer** at the University of Washington, **Hans-Joerg Schulz** at the University of Rostock, **Nils Gehlenborg** at Harvard Medical School, **Torsten Möller** at the University of Vienna, **Tamara Munzener** at the Univeristy of British Columbia, **Helwig Hauser** at the University of Bergen, **Maneesh Agrawala** at UC Berkeley, and **Hendrik Strobelt** at IBM Research.
