---
layout: home
title: Home
menu: Home
order: 1
---

<!-- <img src="assets/i/INFOB2DA2024_logo.png" > -->
<!-- <div class="credits"><a href="http://mbostock.github.io/d3/talk/20111116/bundle.html">Hierarchical edge bundling]</a> | <a href="http://hint.fm/wind/">Wind map</a> | <a href="http://www.nytimes.com/interactive/2012/10/15/us/politics/swing-history.html?_r=0">How states have shifted</a> </div> -->
<div style="width:95%; height:600px; overflow:hidden; position:relative;">
    <img src="assets/i/INFOB2DA2024_logo.png" style="position:absolute; top:-60px; left:-0px;">
</div>

<br><br>

# INFOB2DA | Data Analytics | {{ site.year }}

Course at the Utrecht University

Applied data analytics is a **multidisciplinary field** where you will learn insights needed to make sense of data, research, and observations from everyday life.

You will learn how to apply a **data-driven approach to problem-solving**, but will not only learn about tools, methods, and techniques, or the latest trends, but also more generic insights: why do certain approaches work, why the field is so popular, what common mistakes are made.

The lectures will provide the theoretical background of how a data analytics process should be performed. Furthermore, we discuss an overview of **popular data analytics and visualization techniques** to help match techniques with information needs, including applications of text mining and data enrichment.

## Content

- Fundamental Data Mining Methods
- Data Preparation and Preprocessing
- Common Analysis Algorithms and Methods
- Principles of Information Visualization
- Human Perception and Visualization Design
- Data Visualization Techniques for Particular Data Types

The lecture is separated in three parts. Part one deals with the principal data understanding methods, the second and main focus lies on automatic data preprocessing, cluster & outlier analysis techniques, classification and association rules. Subject of the third part are the basics of information visualization, the foundations of human perception and user interface design.

## Course Sessions and NEWS

> Update 11.06.2024: The initial {{ site.year }} website content has been uploaded; text still under revision; times are not set yet by UU rostering.

### Lectures

_tbd; not defined by UU rostering yet_

<!-- Tuesdays from 05.09.2023 15:15 - 17:00, Location: [KBG - ATLAS](https://students.uu.nl/victor-j-koningsbergergebouw) \
Thursdays 07.09.2023 09:00 - 10:45, Location: [KBG - PANGEA](https://students.uu.nl/victor-j-koningsbergergebouw)
Thursdays from 14.09.2023 09:00 - 10:45, Location: [RUPPERT - 040](https://students.uu.nl/marinus-ruppertgebouw) -->

### Tutorials/Assigments/Labs (werkcollege)

_tbd; not defined by UU rostering yet_

<!-- Mondays from 11.09.2023 17:15 - 19:00, Location: Depending on TA/group assignment (BBG 201, 209) -->

### Office Hours: To be announced (Office hours)

Office hours are posted [here]({{ site.baseurl }}/schedule/#lab_oh_schedule).

**Lecture Resources:**  
Discussion forum on [MS Teams (Discussion Channel)]({{ site.teamsurl }})  
Materials and grades also on [MS Teams (General -> Files)]({{ site.teamsurl }})

**Workload:**

7.5 ECTS-Credits for lecture, tutorials, labs, and homeworks; Representing in total 210 hours, split into

- 50 hours course of study with attendance
- 160 hours of self-study time

## Instructor and Head TF

[Michael Behrisch](http://michael.behrisch.info) (Instructor) \
Alister Machado dos Reis (Head TF)

### Teaching Fellows

Alister Machado dos Reis

Elio Verhoef

Kalee Said

Sacha Vermeer

Vincent Haverhoek

<!-- - _Group 1_ Jasper van Winkelhoff
- _Group 2_ Selim Büyük
- _Group 3_ Hessel Laman
- _Group 4_ Yoram Frenkiel
- _Group 5_ Lisanne Koetsier
  (group allocation subject to change) -->

### COVID-19 Rules for this Class

We are following the [Utrecht University COVID-19 Rules](https://www.uu.nl/en/information-coronavirus).
Generally, we will keep the work as remote as possible, while still trying to foster community building aspect.

**Lectures** will be held ON-SITE; The _Labs/Werkcolleges_ are currently planned to be ON-SITE.  
_Please be aware that this information can change rapidly._

### Previous Years (Archive)

[INFOB2DA 2020 Website](https://infob2da.gitlab.io/2020/)

[INFOB2DA 2021 Website](https://infob2da.gitlab.io/2021/)

[INFOB2DA 2022 Website](https://infob2da.gitlab.io/2022/)

[INFOB2DA 2023 Website](https://infob2da.gitlab.io/2023/)
